*EC521: Development Theory & Policy 
*Mid term paper code 
*Muhammad Bin Salman, BUID: U24927682

set more off 
clear
cd "/Users/mbsalman/Desktop/Dev midterm/data" 
{ //Using Penn World Tables fo Growth Accounting and Solow residual
use pwt100.dta, clear 

encode countrycode, generate(iso)
move iso country
xtset iso year
sort countrycode year
*Countries choosen as Egypt(main), Nigeria (50% less), South Africa (150%)
keep if countrycode=="EGY"|countrycode=="NGA"|countrycode=="ZAF"  | countrycode=="LBN"
*Generates GDP per capita
gen gdppc= cgdpo/pop 
gen log_gdppc= ln(gdppc)
gen n_pop= ln(pop)
*Generate Capital stock per capita at current PPPs(in mil. 2017US$)
gen capital_pop= cn/pop
gen log_cppc= log(capital_pop)
*Generate Human capital, used HDR UN website to construct it. 
gen h_NGA= exp(0.125*6.7) if countrycode=="NGA" 
gen h_ZAF= exp(0.125*10.2) if countrycode=="ZAF" 
gen h_EGY= exp(0.065*7.4) if countrycode=="EGY" 
gen h_LBN= exp(0.065*8.7) if countrycode=="LBN" 
{ // Alternative code: If use labsh and hc for human capital
/*bysort country: tab labsh if year==2019
gen a= 1-labsh 
*Using Labor share from data

gen k_alpha= (capital_pop)^(a)
gen h_alpha= (hc)^(labsh)
*/}
gen h= h_EGY 
replace h= h_ZAF if countrycode=="ZAF"
replace h=h_LBN if  countrycode=="LBN"
replace h=h_NGA if countrycode=="NGA"
gen h_alpha= (h)^(1/3)
gen k_alpha= (capital_pop)^(2/3)
*Solow Residual 
gen A = (gdppc/k_alpha)/h_alpha 
*Graphs for motivation and trends 
xtline gdppc, overlay ytitle(GDP per Capita)
xtline gdppc if year>=1967 & year<2011,overlay 
scatter log_gdppc year|| lfit log_gdppc year, mlabel(country) 
//scatter n_pop year|| lowess n_pop year, by(country)
xtline pop, overlay 
xtline pop if year>=1967, overlay 
xtline gdppc if year>=1967 & year<2011, overlay ytitle(GDP per capita) xtitle(Year)
xtline gdppc if year>=2011, overlay ytitle(GDP per capita) xtitle(Year)
*After the revolution 
xtline capital_pop, overlay
xtline capital_pop if year>=1967 & year<2011, overlay
xtline capital_pop if year>=2010, overlay
xtline gdppc capital_pop
scatter log_gdppc h_alpha if year>=2014|| lfit log_gdppc h_alpha if year>=2014, by(country)
xtline A,overlay ytitle("Solow Residual / Technology")
scatter log_gdppc A || lfit log_gdppc A, by(country)
*Cross country variations
br country gdppc A k_alpha h_alpha if year == 2019

}


{ // Education data from world bank to analyze education 
use "Education data.dta", clear
rename humancapitalindexhciscale01hdhci hci_overall 
rename humancapitalindexhcifemalescale0 hci_female
rename humancapitalindexhcimalescale01h hci_male
rename ïtime year
destring primaryeducationteachersfemalese-hci_male, replace force
encode countrycode, generate(cc)
sort year cc 
move cc countrycode
drop countrycode
rename cc countrycode
encode year, generate(Year)
sort Year countrycode
move Year timecode
//drop timecode
drop in 245/249
drop year
rename Year year
xtset countrycode year, yearly
rename unemploymenttotaloftotallaborfor unemp
rename unemploymentmaleofmalelaborforce unemp_male
rename unemploymentfemaleoffemalelaborf unemp_female
rename pupilteacherratiosecondarysesece pup_teach_ratio_secondary
rename pupilteacherratioprimaryseprmenr pup_teach_ratio_primary
rename literacyrateadulttotalofpeopleag lit_adult_total 
rename literacyrateadultmaleofmalesages lit_adult_male
rename literacyrateadultfemaleoffemales lit_adult_female
rename childrenoutofschoolprimaryseprmu child_ns_p
*Graphs for motivation

**Unemployement 
scatter unemp year || lfit unemp year, by(countryname) xtitle("Year") ytitle("Total Unemployment rate")
scatter unemp_male year || lfit unemp_male year, by(countryname)
scatter unemp_female year || lfit unemp_female year, by(countryname) xtitle("Year") ytitle("Female Unemployment rate")

**Education
scatter pup_teach_ratio_secondary year || lfit pup_teach_ratio_secondary year, by(countryname) ytitle(Pupil-teacher ratio in secondary school) xtitle(Year: Base Year=1960)
scatter pup_teach_ratio_primary year || lfit pup_teach_ratio_primary year, by(countryname)

scatter lit_adult_total year || lfit lit_adult_total year, by(countryname)
scatter lit_adult_male year || lfit lit_adult_male year, by(countryname)
scatter lit_adult_female year || lfit lit_adult_female year, by(countryname)

scatter child_ns_p year || lfit child_ns_p year, by(countryname)
}
{ //Investments 
use "investments.dta", clear
rename ïtime year
encode countrycode, generate(cc)
move cc countrycode
drop countrycode
rename cc countrycode
encode year, generate(Year)
sort Year countrycode
move Year timecode
//drop timecode
drop year
rename Year year
xtset countrycode year, yearly
destring foreigndirectinvestmentnetbopcur foreigndirectinvestmentnetinflow, replace force
drop in 245/249
gen log_FDI_inflow= ln(foreigndirectinvestmentnetinflow)
**Graphs for motivation 
scatter log_FDI_inflow year || lfit log_FDI_inflow year, by(countryname)
scatter log_FDI_inflow year|| lowess log_FDI_inflow year, by(countryname) ytitle("FDI Inflow") xtitle(Year: Base Year=1960)
scatter log_FDI_inflow year if year>=50|| lowess log_FDI_inflow year if year>=50, by(countryname) ytitle("FDI Inflow") xtitle(Year: Base Year=1960)

}
